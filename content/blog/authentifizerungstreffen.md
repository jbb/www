---
title: "Authentifizierungs-Treffen"
tags: ['keysigning']
date: 2007-01-13T02:00:00+00:00
draft: false

---
## Zeit & Ort
* Dienstag, 23. Januar 07 um 17.45 Uhr
* Informatik-Gebäude, Takustraße 9, Raum 049^h^h^h !!!!!NEIN!! Wegen Raumdieben auf Raum 053 verlegt!!!!

## Wieso Verschlüsseln?
In Zeiten, in denen Millionen für multimediale Überwachungstechniken ausgegeben werden, Mailbetreiber verpflichtet sind, Schnittstellen zur einfachen Überwachung einzurichten, unsere Mails über die ganze Welt verschickt und ausgewertet werden, kommt trotzdem häufig die Aussage "Ich habe doch nichts zu verbergen".

Ich glaube, dass diese Aussage auf Naivität und Phantasielosigkeit beruht und keiner dieser Menschen bei einer staatlichen oder gar gewerblichen Umfrage alle seine Daten der Reihe nach aufschreiben würde.

## Wieso ein Authentifizierungs-Treffen?
Eine Keysigning-Party erlaubt allen Teihnehmern ein Vertrauensnetz aufzubauen, in diesem Netz kann die Kommunikation sicher und geheim sein. An diesem Treffen werden viele Personen teilnehmen, die schon ein Vertrauensnetz haben und Du wirst an Ihren Vertrauensnetzen teilnehmen können.

* [Netz vom 6. Juli 06](/images/blog/web-of-trust-july-2006.png)
* [Netz vom 3. März 06](/images/blog/web-of-trust-jan-2006.png)

Siehe auch: http://de.wikipedia.org/wiki/Web_of_Trust

## Was brauche ich für das Treffen?

Um aktiv teilnehmen zu können, wird ein Schlüsselpaar gebraucht. Zur Erzeugung siehe
* eine ausführliche deutsche und englische Anleitung
* Anleitung für gpg unter Windows (auch mit GUI)
* mit Seahorse oder GPA unter Linux.
Wir empfehlen bei der Schlüsselerzeugung keine Mailadresse anzugeben, da wir die Mailadresse eh nicht verifizieren und sie erfahrungsgemäss von Spammern benutzt wird.
Dieser Schlüssel muss vor dem Treffen an die Adresse bogus-gpg [a] spline.inf.fu-berlin.de gesendet werden.
Zum Treffen muss ein
* Ausdruck der eigenen Schlüssel-ID und des Schlüssel-Fingerprints
* Lichtbildausweis
mitgebracht werden.

## Ablauf
Die Keysigning-Party wird in drei Phasen durchgeführt.
* Die Teilnehmer erhalten die Schlüssel-ID und -Fingerprints aller anderen Teilnehmer als Ausdruck. Es wird geprüft, ob alle Ausdrucke gleich sind.
* In der zweiten Phase werden die Teilnehmer der Reihe nach aufgerufen und gebeten, den anderen ihren Personalausweiß zu zeigen, so dass alle die Identität bestätigen können. Stimmt die Identität überein, markieren die Teilnehmer die Schlüssel-ID und den entsprechenden Fingerprint.

Nachdem alles überprüft und markiert wurde, ist der offizielle Teil beendet. Die entscheidende dritte Phase, das eigentliche Keysigning, wird von jedem Teilnehmer zu Hause und alleine durchgeführt.
* Jeder Teilnehmer importiert und signiert mit seinem privaten GPG-Schlüssel alle Schlüssel, die bei dem Treffen als richtig markiert wurden. Anschliessend sendet er die signierten Schlüssel an einen öffentlichen Schlüsselserver oder per Mail an den Besitzer zurück. Eine deutsche und englische Anleitung zum Signieren von GPG-Schlüsseln ist ebenfalls Teil des GnuPG-HowTos.

**UPDATE:** entstandene Web-of-Trust (30.01.2007)

![Web-of-Trust](/images/blog/web-of-trust-jan-2007.png)
