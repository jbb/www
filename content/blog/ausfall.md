---
title: "Serverprobleme gestern"
tags: ['dienste', 'spline']
date: 2014-09-13T02:00:00+00:00
draft: false

---
Gestern vormittag (Fr 11.09.2014) gab es über einige Stunden einen Ausfall von vielen Spline-Diensten. Grund war, dass einer unserer beiden Hauptserver komplett ausgefallen war. Gegen 15 Uhr waren wieder alle Dienste verfügbar.

Falls es noch irgendwo Probleme gibt, gebt uns bitte Bescheid an **spline@spline.de**.

Wir möchten uns für die entstandenen Unannehmlichkeiten entschuldigen und bitten um Verständnis, dass es etwas länger dauerte – die gesamte Infrastruktur von spline.de wird von uns vollständig in unserer Freizeit verwaltet.
