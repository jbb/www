---
title: Graphen visualisieren mit Graphviz
location: Hörsaal der Informatik
time: 18:00 Uhr
date: 2010-04-26
---
Von der Breitensuche zum Zustandsautomaten, vom Syntaxbaum zum Netzwerkdiagramm
-- immer wieder kommt man in die Situation, eine ästhetisch ansprechende
Darstellung eines gegebenen Graphen erzeugen zu wollen. Das Graphviz-Paket
bietet eine Lösung für dieses Problem und stellt dazu verschiedene
Graph-Drawing-Algorithmen und viele Optionen zur Layout-Steuerung bereit. In
diesem Vortrag soll eine Einführung in den Umgang mit Graphviz gegeben und die
grundlegenden Optionen vorgestellt werden.

## Vortragende
[Tobias Pfeiffer](https://www.mi.fu-berlin.de/w/Main/TobiasPfeiffer)

## Slides
[graphviz.pdf](/talks/graphviz.pdf)

## Links
* [WP: Graphzeichnen](http://de.wikipedia.org/wiki/Graphzeichnen)
* [Graphviz](http://www.graphviz.org/)
* [WP: DOT (Graphviz)](http://de.wikipedia.org/wiki/DOT_(GraphViz))
* [dot2tex](http://www.fauskes.net/code/dot2tex/)
* [PGF/TikZ (LaTeX-Grafikpaket)](http://sourceforge.net/projects/pgf/)