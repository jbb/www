---
title: Einführung in die Programmiersprache Lua
location: SR005 (Takustr. 9)
time: 18:00 Uhr
date: 2015-12-08T12:00:00+01:00
---
Lua ist eine imperative und als Skriptsprache erweiterbare Programmiersprache. Einsatz findet Lua vor allem bei der Entwicklung Compuerspielen, um z.B. die KI eines Spieler, mods oder vieles mehr vom Gaming-Engine zu trennen. Doch auch in Software wie wireshark, nmap oder vlc kommt Lua zum Einsatz.

Der Talk beinhaltet eine Einführung in die Sprache Lua und geht weiter zu Metatables bis hin zur Einbindung eigener C-Libs. 

Alex Gordon ist ein aktives Spline-Mitglied und arbeitet seit einiger Zeit mit Lua. Er entwickelte bereits mods für Spiele wie Minetest.

## Vortragende
Alex Gordon